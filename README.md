CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration



INTRODUCTION
------------
The Cellular ID module provides capability to enter, convert, store, and display
many different formats of cellular device ID numbers.  This capability is exposed
in several ways:
* A Field API field that you can attach to any entity
* An AJAX-enabled ID converter block
* Several feild formattters


This module bundles the excellent DeviceIdConverter PHP class from
https://github.com/rfx1007/DeviceIdConverter



REQUIREMENTS
------------
This module utilizes the Field API in core.



INSTALLATION
------------
Install as you would normally install a contributed Drupal module.
See: https://drupal.org/documentation/install/modules-themes/modules-7
for further information.



CONFIGURATION
-------------
The module has no menu or modifiable settings. There is no configuration. When
enabled, the Cellular ID field will be available in the 'Manage Fields' UI, and
the 'Cellular ID Converter' block will appear in the blocks admin page.

