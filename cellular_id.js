(function ($) {
  // Behavior for the cellular ID conversion block.
  Drupal.behaviors.CellularId = {
    attach: function (context, settings) {
      $('#cellular-id-input').keyup(function(event) {
        if (event.which == 13) {
          $('input.cellular-id-convert-button').mousedown();
        }
      });
    }
  }
})(jQuery);
